# Installation

## Instructions

  To compile our proofs, you need `opam`, the OCaml package manager.
    (A recent version of `opam` is recommended; we use 2.0.6.)

  Once you have `opam`, please type:

    ./setup.sh

  This should create a "local opam switch" in the current directory, install
  the *specific* versions of Coq, stdpp, and Iris that we need, then
  compile our proofs.

## Troubleshooting

  In case you need to know, information on the specific versions of
  Stdpp and Iris that we need can be found in the file `opam`. This
  development is known to compile with Coq 8.11.2.

  `opam` may complain that the repository `iris-dev` already exists
  with a different URL. If this happens, please type

    opam repo remove iris-dev --all

  then try `./setup.sh` again. (You will later need to re-declare
  the `iris-dev` repository. Sorry about that.)

  Since version 2.0, `opam` sandboxes package commands by the default.
  In MacOS, it uses the `sandbox_exec` utility, which might complain
  about the call to `execvp` in `ocaml`'s installation script. We have
  found that installing a global switch, rather than the default local
  switch, solves this problem. To create a global switch, it suffices
  to choose a `<name>` for this switch, and to call the setup script
  with `<name>` as an argument:

    ./setup.sh <name>

  The switch `<name>` will then be included in the list of global switches.
  You can later remove this switch with the following command:

    opam switch remove <name>

