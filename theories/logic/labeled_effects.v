(* labeled_effects.v *)

From iris.proofmode Require Import base tactics classes.
From logic          Require Import extended_weakestpre notation tactics
                                   shallow_handler.
From lib            Require Import lists.

Set Default Proof Using "Type".


(** * Implementation. *)

Section implementation.

Definition effect : val := λ: <>, Alloc #().

Definition perform : val := λ: "l" "v", do:("l", "v").

Definition shandle : val := rec: "shandle" "l" "client" "h" "r" :=
  TryWith ("client" #())
  (* Effect branch: *)
    (λ: "args" "k",
      let: "l'" := Fst "args" in
      let: "v"  := Snd "args" in
      if: "l" = "l'" then
        "h" "v" "k"
      else
        let: "y" := (do: "args") in
        "shandle" "l" (λ: <>, "k" "y") "h" "r")
  (* Return branch: *)
    (λ: "res", "r" "res").

Definition handle (n : expr) : expr := rec: "handle" "client" "h" "r" :=
  shandle n "client"
  (* Effect branch: *)
    (λ: "v" "k",
      "h" "v" (λ: "w", "handle" (λ: <>, "k" "w") "h" "r"))
  (* Return branch: *)
    (λ: "res", "r" "res").

Definition catch_all : val := rec: "catch_all" "client" "h" "r" :=
  TryWith ("client" #())
  (* Effect branch: *)
    (λ: "args" "k",
      let: "s" := Fst "args" in
      let: "x" := Snd "args" in
      let: "k_with_catch_all" := λ: "w",
        "catch_all" (λ: <>, "k" "w") "h" "r"
      in
      "h" (λ: "x", perform "s" "x") "x" "k_with_catch_all")
  (* Return branch: *)
    (λ: "res", "r" "res").

Definition try_finally : val := λ: "e₁" "e₂",
  TryWith ("e₁" #()) (λ: <> <>, "e₂" #()) (λ: <>, "e₂" #()).

Definition dynamic_wind : val := λ: "pre" "body" "post",
  "pre" #();;
  catch_all "body"
  (* Effect branch: *)
    (λ: "do" "x" "k",
       "post" #();;
       let: "w" := "do" "x" in
       "pre" #();;
       "k" "w")
  (* Return branch: *)
    (λ: "y", "post" #();; "y").

End implementation.


(** * Logical Definitions. *)

(** Abbreviations. *)

Definition eff_label  : Type := loc.
Definition eff_list Σ : Type := list (eff_label * pure_iEff Σ).

Definition eff_labelO := locO.
Canonical Structure eff_listO Σ :=
  list.listO (prodO eff_labelO (pure_iEffO (Σ:=Σ))).


(** Protocol. *)

Section protocol.
  Context `{!heapG Σ}.

  (* [eff l] asserts the full ownership of a fresh effect label. *)
  Definition eff (l : loc) : iProp Σ := l ↦ #().

  (* [Eff L] asserts that the effect labels have been allocated. *)
  Definition Eff (L : list.listO eff_labelO) : iProp Σ :=
    ([∗ list] l ∈ L, l ↦□ #())%I.

  (* [EFF E] is the protocol describing the effects of programs written in
     an idiom of [HH] where an effect is always the pair of a label [l] and
     a value [v]. *)
  Definition EFF (E : eff_listO Σ) : iEff Σ :=
    (>> (l : eff_label) (v : val) (Ψ : pure_iEff Σ) (Q : val → iProp Σ) >>
       ! (#l, v)%V     {{ ⌜ (l, Ψ) ∈ E ⌝ ∗ iEff_car Ψ v Q }};
     << (w : val) <<
       ? (w)           {{ Q w }})%ieff.

  Lemma EFF_agreement v' E Φ : protocol_agreement v' (EFF E) Φ ≡
    (∃ (l : eff_label) (v : val) (Ψ : pure_iEff Σ) (Q : val → iProp Σ),
      ⌜ v' = (#l, v)%V ⌝ ∗
      (⌜ (l, Ψ) ∈ E ⌝ ∗ iEff_car Ψ v Q) ∗
      □ (∀ (w : val), Q w -∗ Φ w))%I.
  Proof.
    rewrite /EFF (protocol_agreement_tele' [tele _ _ _ _] [tele _]). by auto.
  Qed.

  Lemma iEff_car_EFF E v' Q' : iEff_car (EFF E) v' Q' ⊣⊢
    (∃ (l : eff_label) (v : val) (Ψ : pure_iEff Σ) (Q : val → iProp Σ),
       ⌜ v' = (#l, v)%V ⌝ ∗
       (⌜ (l, Ψ) ∈ E ⌝ ∗ iEff_car Ψ v Q) ∗
       □ (∀ (w : val), Q w -∗ Q' w)%I).
  Proof. by rewrite /EFF (iEff_tele_eq' [tele _ _ _ _] [tele _]). Qed.

  Global Instance Eff_ne : NonExpansive Eff.
  Proof.
    intros n E E' HE. unfold Eff. repeat f_equiv.
    apply leibniz_equiv.
    apply (@discrete (list.listO eff_labelO) n E);[
    by apply _| by rewrite HE].
  Qed.

  Global Instance Eff_proper : Proper ((≡) ==> (≡)) Eff.
  Proof.
    by intros ???; apply equiv_dist=>n; apply Eff_ne; apply equiv_dist.
  Qed.

  (* If every effect label in [E] is associated to a pure protocol,
     then [EFF E] is a pure protocol. *)
  Instance EFF_pure_protocol E :
    NoDup E.*1 →
      PureProtocol (EFF E).
  Proof.
    intros HND.
    constructor. intros v'.

    (* The value [v'] is either a valid request with respect to the protocol
       [EFF E], in which case there exists a value [v], a label [l], and a
       protocol [Ψ] such that [v' = (#l, v)%V] and [(l, Ψ) ∈ E], or an invalid
       request, in which case [v'] is not of the form [(#l, v)%V] for any
       label [l] and value [v]. *)
    set (is_lbl_val :=
      λ (v : val) (l : eff_label),
        v' = (#l, v)%V).
    set (is_valid_request :=
      λ (v : val) (l : eff_label) (Ψ : pure_iEff Σ),
        is_lbl_val v l ∧ (l, Ψ) ∈ E).

    assert ((* (1) *) (∃ v l Ψ,   is_valid_request v l Ψ) ∨
            (* (2) *) (∀ v l Ψ, ¬ is_valid_request v l Ψ))
      as [[v [l [Ψ [Hlbl Hin]]]] | Hneq].
    { assert ((* (a) *) (∃ v l,   is_lbl_val v l) ∨
              (* (b) *) (∀ v l, ¬ is_lbl_val v l)) as [[v [l Heq]] | Hneq].
      { unfold is_lbl_val.
        destruct v'; try destruct v'1, v'2;
        try destruct l; try naive_solver.
      }
      (* (a) *)
      { case (decide (l ∈ E.*1)).
        { rewrite elem_of_list_fmap.
          intros [(l', Ψ) [-> Hin]].
          left. by exists v, l', Ψ.
        }
        { intros Hnotin. right. intros w l' Ψ [Heq' Hin].
          unfold is_lbl_val in Heq, Heq'.
          simplify_eq. apply Hnotin.
          rewrite elem_of_list_fmap. by exists (l', Ψ).
        }
      }
      (* (b) *)
      { right. intros ??? [??]. by apply (Hneq v l). }
    }

    (* (1) *)
    - unfold is_lbl_val in Hlbl. simplify_eq.
      destruct (@pure_protocol _ _ (pure_iEff_pure _ Ψ) v) as [Q HQ].
      exists Q. iIntros (Q') "HE".
      rewrite iEff_car_EFF.
      iDestruct "HE" as "[%l' [%v' [%Ψ' [%Q'' (%Heq & [%Hl' H'] & #HQ')]]]]".
      injection Heq. intros -> ->.
      rewrite -(NoDup_fmap_fst_Inj _ _ _ _ HND Hin Hl').
      iPoseProof (HQ with "H'") as "[HQ HQ'']".
      iSplitR "HQ''".
      + rewrite iEff_car_EFF.
        iExists l', v', Ψ, Q; by auto.
      + iIntros (w) "HQ".
        iDestruct ("HQ''" with "HQ") as "#HQ''".
        iModIntro. by iApply "HQ'".

    (* (2) *)
    - exists (λ _, True)%I. iIntros (Q') "HE".
      rewrite iEff_car_EFF.
      iDestruct "HE" as "[%l [%v [%Ψ [%Q (-> & [%Hl _] & _)]]]]".
      by destruct (Hneq v l Ψ).
  Qed.

End protocol.


(** Weakest precondition. *)

Section weakest_precondition.
  Context `{!heapG Σ}.

  (* [EffLabels L] holds if [L] is a list of allocated
     effect labels without duplicates. *)
  Definition EffLabels (L : list eff_label) : iProp Σ :=
    ⌜ NoDup L ⌝ ∧ Eff L.

  (* We define a notion of weakest precondition tailored for this idiom
     of [HH] where an effect carries a name. *)
  Definition wp_def :
    expr -d> eff_listO Σ -d> (val -d> iPropO Σ) -d> iPropO Σ :=
    (λ e E Φ,
      EffLabels (E.*1) -∗
        EWP e <| EFF E |> {{ y, Φ y }})%I.

  Global Instance wp_ne e E n :
    Proper ((dist n) ==> (dist n)) (wp_def e E).
  Proof. unfold wp_def. intros => ???. by repeat (f_equiv; try intros ?). Qed.

  Global Instance wp_proper e E : Proper ((≡) ==> (≡)) (wp_def e E).
  Proof. by intros ???; apply equiv_dist=>n; apply wp_ne; apply equiv_dist. Qed.

End weakest_precondition.


Notation "'WP' e <| E '|' '>' {{ Φ  } }" :=
  (wp_def e%E E Φ)
  (at level 20, e, E, Φ at level 200).

Notation "'WP' e <| E '|' '>' {{ v , Φ } }" :=
  (wp_def e%E E (λ v, Φ)%I)
  (at level 20, e, E, Φ at level 200).


(** * Reasoning Rules. *)

(** Auxiliary lemmas about [eff] and [Eff]. *)

Section auxiliary_lemmas.
  Context `{!heapG Σ}.

  Implicit Types l : eff_label.
  Implicit Types E : @eff_list Σ.
  Implicit Types Ψ : iEff Σ.

  Lemma Eff_cons l L : Eff (l :: L) = (l ↦□ #() ∗ Eff L)%I.
  Proof. by unfold Eff. Qed.

  Lemma distinct_eff_name l L :
    eff l -∗ Eff L -∗ ⌜ l ∉ L ⌝%I.
  Proof.
    iIntros "Hs HS".
    iInduction L as [|l' L Hnot_in] "IH".
    - iPureIntro. by apply not_elem_of_nil.
    - rewrite Eff_cons not_elem_of_cons.
      iDestruct "HS" as "[Hs' HS]". iSplit.
      { by iApply (pointsto_ne with "Hs Hs'"). }
      { by iApply ("IH" with "Hs HS"). }
  Qed.

  Lemma Eff_incl L L' : L ⊆ L' → Eff L' -∗ Eff L.
  Proof.
    induction L as [|l L]; unfold Eff.
    - by iIntros (HSub) "?".
    - iIntros (Hsub) "#HS'".
      iApply big_sepL_cons.
      by iSplit;[
      iApply (big_sepL_elem_of with "HS'")|
      iApply (IHL with "HS'")];
      set_solver.
  Qed.

  Lemma Eff_app L L' : Eff L -∗ Eff L' -∗ Eff (L ++ L').
  Proof.
    rewrite /Eff. iIntros "#HL #HL'".
    iInduction L as [|l L] "IH"; [done|]. simpl.
    iDestruct "HL" as "[$ HL]". by iApply "IH".
  Qed.

  Lemma EffLabels_nil : ⊢ EffLabels [].
  Proof. by iSplit; [iPureIntro; apply NoDup_nil|unfold Eff]. Qed.

  Lemma EffLabels_incl L L' : L ⊆+ L' → EffLabels L' -∗ EffLabels L.
  Proof.
    iIntros (Hsub) "[%HND #HL']". iSplit.
    - iPureIntro.
      by apply (submseteq_NoDup L L').
    - iApply (Eff_incl with "HL'").
      by apply submseteq_subseteq.
  Qed.

  Lemma EFF_empty : ⊢ (EFF ([] : eff_list Σ)  ⊑ ⊥)%ieff.
  Proof.
    iIntros "!#" (v Φ).
    rewrite !EFF_agreement.
    iDestruct 1 as (l' v' Ψ' Q) "(_ & (%H & _) & _)".
    by inversion H.
  Qed.

  Lemma EFF_weaken l (Ψ Ψ' : pure_iEff Σ) E :
    (Ψ ⊑ Ψ')%ieff -∗
      (EFF ((l, Ψ) :: E) ⊑ EFF ((l, Ψ') :: E))%ieff.
  Proof.
    iIntros "#Hle !>" (v Φ) "HE".
    rewrite !EFF_agreement.
    iDestruct "HE" as (s' v' Ψ'' Q) "(-> & (% & HΨ'') & #Hk)".
    revert H. rewrite elem_of_cons. intros [Heq|Hin].
    - injection Heq. intros -> ->.
      iDestruct ("Hle" $! v' Q with "[HΨ'']") as "[%Q' [HΨ #HQ]]".
      { iExists Q. iFrame. by iIntros "!#" (?) "$". }
      iExists l, v', Ψ', Q'. iFrame. iSplit; [done|].
      iSplit; [iPureIntro; by set_solver|].
      iIntros "!#" (w) "HQ'". iApply "Hk".
      by iApply "HQ".
    - iExists s', v', Ψ'', Q. iFrame. iSplit; [done|].
      by iSplit; [iPureIntro; set_solver|].
  Qed.

  Lemma EFF_introduce s E : ⊢ (EFF ((s, ⊥%ieff) :: E) ⊑ EFF E)%ieff.
  Proof.
    iIntros "!>" (v Φ) "HE".
    rewrite !EFF_agreement.
    iDestruct "HE" as (s' v' Ψ' Q) "(-> & (% & HΨ') & #Hk)".
    revert H. rewrite elem_of_cons. intros [Heq|Hin].
    - injection Heq. by intros -> ->.
    - iExists s', v', Ψ', Q. iFrame.
      by iSplit; [|iSplit].
  Qed.

  Lemma EFF_incl E E' : E ⊆ E' → ⊢ (EFF E ⊑ EFF E')%ieff.
  Proof.
    iIntros (Hincl v Φ) "!# HE".
    rewrite !EFF_agreement.
    iDestruct "HE" as (s' v' Ψ' Q) "(-> & (% & HΨ') & #Hk)".
    iExists s', v', Ψ', Q. iFrame.
    iSplit; [|iSplit]; try done.
    iPureIntro. by set_solver.
  Qed.

  Lemma EFF_app_l E1 E2 E :
    (EFF E1 ⊑ EFF E)%ieff -∗
      (EFF E2 ⊑ EFF E)%ieff -∗
        (EFF (E1 ++ E2) ⊑ EFF E)%ieff.
  Proof.
    iIntros "#HE1 #HE2 !#" (v Ψ).
    rewrite EFF_agreement.
    iIntros "[%l [%v' [%Ψ' [%Q (-> & (%Hin & HiEff) & #H)]]]]".
    revert Hin; rewrite elem_of_app; intros [Hin|Hin];
    [iApply "HE1"|iApply "HE2"];
    rewrite EFF_agreement;
    iExists l, v', Ψ', Q;
    iFrame; repeat iSplit; done.
  Qed.

  Lemma EFF_app_r1 E1 E2 :
    ⊢ (EFF E1 ⊑ EFF (E1 ++ E2))%ieff.
  Proof. by iApply EFF_incl; set_solver. Qed.

  Lemma EFF_app_r2 E1 E2 :
    ⊢ (EFF E2 ⊑ EFF (E1 ++ E2))%ieff.
  Proof. by iApply EFF_incl; set_solver. Qed.

  Lemma EFF_app E1 E1' E2 E2' :
    (EFF E1 ⊑ EFF E1')%ieff -∗
      (EFF E2 ⊑ EFF E2')%ieff -∗
        (EFF (E1 ++ E2) ⊑ EFF (E1' ++ E2'))%ieff.
  Proof.
    iIntros "#HE1' #HE2'".
    iApply EFF_app_l.
    - by iApply iEff_le_trans; [iApply "HE1'"|iApply EFF_app_r1].
    - by iApply iEff_le_trans; [iApply "HE2'"|iApply EFF_app_r2].
  Qed.

End auxiliary_lemmas.


(** Main reasoning rules: value, step, and bind rules. *)

Section reasoning_rules.
  Context `{!heapG Σ}.

  Lemma wp_value E Φ v : Φ v ⊢ WP of_val v <| E |> {{ Φ }}.
  Proof. iIntros "HΦ [%H HS]". iApply ewp_value. by iFrame. Qed.

  Lemma wp_pure_step e e' E Φ :
    pure_prim_step e e' → 
      ▷ WP e' <| E |> {{ Φ }} ⊢ WP e <| E |> {{ Φ }}.
  Proof.
    iIntros "% He' [% HL]".
    iApply (ewp_pure_step' _ _ e'); first done.
    iNext. iApply "He'". by iSplit.
  Qed.

  Lemma wp_bind K `{NeutralEctx K} E Φ e e' :
    e' = fill K e  →
      WP e  <| E |> {{ v, WP fill K (of_val v) <| E |> {{ Φ }} }} ⊢
      WP e' <| E |> {{ Φ }}.
  Proof.
    iIntros (->) "He [% #HL]".
    iSpecialize ("He" with "[HL]"). { by iSplit. }
    iApply (ewp_bind K); first done.
    iApply (ewp_strong_mono with "He"). done.
    { by iApply iEff_le_refl. }
    iIntros "!#" (v) "HK !>".
    iApply "HK". by iSplit.
  Qed.

  Lemma fupd_wp e E Φ :
    TCEq (to_eff e) None →
    (|==> WP e <| E |> {{ Φ }}) ⊢ WP e <| E |> {{ Φ }}.
  Proof.
    iIntros (?) "He [%HND HE]". iApply fupd_ewp.
    iMod "He". iModIntro.
    iApply ("He" with "[HE]"). by iSplit.
  Qed.

End reasoning_rules.


(** Tactics. *)

Ltac match_wp_goal lemma tac :=
  match goal with
  | [ |- @bi_emp_valid _                (wp_def ?e _ _) ] => tac lemma e
  | [ |- @environments.envs_entails _ _ (wp_def ?e _ _) ] => tac lemma e
  end.

Ltac wp_pure_step_lemma :=
  iApply wp_pure_step.

Ltac wp_bind_rule_lemma K :=
  iApply (wp_bind K).

Ltac wp_pure_step :=
  match_wp_goal wp_pure_step_lemma pure_step_tac.

Ltac wp_bind_rule :=
  match_wp_goal wp_bind_rule_lemma bind_rule_tac.

Ltac wp_value_or_step :=
  ((iApply wp_value) || (wp_bind_rule; wp_pure_step));
  try iNext; simpl.

Ltac wp_pure_steps :=
  repeat wp_value_or_step.


(** Monotonicity. *)

Section monotonicity.
  Context `{!heapG Σ}.

  Lemma wp_mono E Φ Φ' e :
    □ (∀ v, Φ v ==∗ Φ' v) -∗
      WP e <| E |> {{ Φ }} -∗
        WP e <| E |> {{ Φ' }}.
  Proof.
    iIntros "#HΦ' He [%HND HE]".
    iSpecialize ("He" with "[HE]"). { by iSplit. }
    iApply (ewp_strong_mono with "He"); first done.
    { by iApply iEff_le_refl. }
    iIntros "!#" (y) "H".
    by iMod ("HΦ'" with "H") as "$".
  Qed.

  Lemma wp_mono_pure E Φ Φ' e :
    (∀ v, Φ v ==∗ Φ' v) -∗
      WP e <| [] |> {{ Φ  }} -∗
        WP e <| E |> {{ Φ' }}.
  Proof.
    iIntros "HΦ' He [%HND HE]".
    iSpecialize ("He" with "[]").
    { iSplit; [|by unfold Eff].
      iPureIntro. by apply NoDup_nil_2.
    }
    iApply (ewp_mono_pure' _ _ Φ with "[He]").
    { iApply (ewp_strong_mono with "He"). done.
      { by iApply EFF_empty. }
      { by iIntros "!#" (v) "$". } }
    { iIntros (y) "Hy".
      by iMod ("HΦ'" with "Hy") as "$". }
  Qed.

  Lemma wp_eff_frame E E' Φ e :
    E ⊆+ E' →
      WP e <| E |> {{ Φ }} -∗
        WP e <| E' |> {{ Φ }}.
  Proof.
    iIntros (Hsubm) "He [%HND' #HE']".
    specialize (fmap_submseteq fst _ _ Hsubm) as Hsubm'.
    specialize (submseteq_NoDup _ _ Hsubm' HND') as HND.
    iPoseProof (Eff_incl with " HE'") as "#HE".
    { by apply (submseteq_subseteq _ _ Hsubm'). }
    iSpecialize ("He" with "[HE]"). { by iSplit. }
    (* Proof by monotonicity of [ewp]. *)
    iApply (ewp_strong_mono with "He");
    first done;
    last by iIntros "!#" (y) "H".
    iApply EFF_incl.
    { by apply (submseteq_subseteq _ _ Hsubm). }
  Qed.

  Corollary wp_eff_app_r E E' Φ e :
    WP e <| E |> {{ Φ }} -∗
      WP e <| E ++ E' |> {{ Φ }}.
  Proof.
    iIntros "He".
    iApply (wp_eff_frame with "He").
    apply (submseteq_inserts_r E').
    by set_solver.
  Qed.

  Corollary wp_eff_app_l E E' Φ e :
    WP e <| E |> {{ Φ }} -∗
      WP e <| E' ++ E |> {{ Φ }}.
  Proof.
    iIntros "He".
    iApply (wp_eff_frame with "He").
    apply (submseteq_inserts_l E').
    by set_solver.
  Qed.

  (* The order of effect names does not matter. *)
  Corollary wp_eff_permute E E' Φ e :
    E ≡ₚ E' →
      WP e <| E |> {{ Φ }} -∗
        WP e <| E' |> {{ Φ }}.
  Proof.
    intros Hp. iApply wp_eff_frame.
    by apply Permutation_submseteq.
  Qed.

  Corollary wp_strong_mono E E' Φ Φ' e :
    E ⊆+ E' →
      □ (∀ v, Φ v ==∗ Φ' v) -∗
        WP e <| E |> {{ Φ }} -∗
          WP e <| E' |> {{ Φ' }}.
  Proof.
    iIntros (Hsub) "#HΦ He".
    iApply (wp_eff_frame _ _ _ _ Hsub).
    by iApply (wp_mono with "HΦ He").
  Qed.

  Lemma wp_weaken l E (Ψ Ψ' : pure_iEff Σ) Φ e :
    (Ψ ⊑ Ψ')%ieff -∗
      WP e <| (l, Ψ) :: E |> {{ Φ }} -∗
        WP e <| (l, Ψ') :: E |> {{ Φ }}.
  Proof.
    iIntros "#Hle He [% HE]".
    iSpecialize ("He" with "[HE]"). { by iSplit. }
    (* Proof by monotonicity of [ewp]. *)
    iApply (ewp_strong_mono with "He");
    first done;
    last by iIntros "!#" (y) "?".
    by iApply EFF_weaken.
  Qed.

  Corollary wp_dismiss l E Ψ Φ e :
    WP e <| (l, ⊥) :: E |> {{ Φ }} -∗
      WP e <| (l, Ψ) :: E |> {{ Φ }}.
  Proof. iApply wp_weaken. by iApply iEff_le_bottom. Qed.

End monotonicity.


(** Heap-manipulating operations. *)

Section heap.
  Context `{!heapG Σ}.

  Lemma wp_alloc E Φ v :
    ▷ (∀ (l : loc), l ↦ v ={⊤}=∗  Φ #l) -∗
      WP ref v <| E |> {{ Φ }}.
  Proof.
    iIntros "HΦ _".
    iApply ewp_alloc.
    iIntros "!>" (l) "Hl".
    by iApply ("HΦ" with "Hl").
  Qed.

  Lemma wp_load E Φ l q v :
    ▷ l ↦{q} v -∗
      ▷ (l ↦{q} v ={⊤}=∗ Φ v) -∗
        WP (Load #l)%E <| E |> {{ Φ }}.
  Proof.
    iIntros "Hl HΦ _".
    iApply (ewp_load with "Hl").
    iNext. iIntros "Hl".
    by iApply ("HΦ" with "Hl").
  Qed.

  Lemma wp_store E Φ l v w :
    ▷ l ↦ v -∗
      ▷ (l ↦ w ={⊤}=∗ Φ #()) -∗
        WP (#l <- w)%E <| E |> {{ Φ }}.
  Proof.
    iIntros "Hl HΦ _".
    iApply (ewp_store with "Hl").
    iNext. iIntros "Hl".
    by iApply ("HΦ" with "Hl").
  Qed.

End heap.


(** Client side. *)

(* Reasoning rules for allocating a fresh effect name,
   performing an effect, and introducing a name to the
   list of effects. *)

Section effects.
  Context `{!heapG Σ}.

  Lemma wp_effect E Φ :
    (∀ (l : eff_label), eff l -∗ Φ #l) -∗
      WP effect #() <| E |> {{ Φ }}.
  Proof.
    iIntros "HPost [% HEff]".
    unfold effect. ewp_pure_steps.
    iApply ewp_alloc.
    iIntros "!>" (l) "Hl".
    iModIntro. iFrame. by iApply "HPost".
  Qed.

  Lemma wp_perform (l : eff_label) (v : val) E (Ψ : pure_iEff Σ) Φ :
    (l, Ψ) ∈ E →
      protocol_agreement v Ψ Φ -∗
        WP (perform #l v) <| E |> {{ Φ }}.
  Proof.
    iIntros (?) "Hprot [% HEff]". rename H into Hin.
    unfold perform. ewp_pure_steps.
    iApply ewp_eff. rewrite EFF_agreement.
    iDestruct "Hprot" as (Q) "[HP #Hk]".
    iExists l, v, Ψ, Q. iSplit; [done|]. iFrame.
    iSplit; [done|].
    iIntros "!#" (w) "HQ". iNext.
    iApply ewp_value.
    by iApply "Hk".
  Qed.

  Lemma wp_perform' (l : eff_label) E (Ψ : pure_iEff Σ) Φ :
    (l, Ψ) ∈ E →
      (∀ (f : val),
        □ (∀ v Φ',
          protocol_agreement v Ψ Φ' -∗ WP f v <| E |> {{ Φ' }}) -∗
        Φ f) -∗
          WP (perform #l) <| [] |> {{ Φ }}.
  Proof.
    iIntros "%Hin HΦ _".
    unfold perform. ewp_pure_steps.
    iApply "HΦ".
    iIntros "!#" (x Φ') "Hprot".
    iIntros "[% Heff]". ewp_pure_steps.
    iApply ewp_eff. rewrite EFF_agreement.
    iDestruct "Hprot" as (Q) "[HP #Hk]".
    iExists l, x, Ψ, Q. iSplit; [done|]. iFrame.
    iSplit; [done|].
    iIntros "!#" (w) "HQ". iNext.
    iApply ewp_value.
    by iApply "Hk".
  Qed.

  Lemma wp_introduce l E Φ e :
    TCEq (to_eff e) None →
      WP e <| (l, ⊥) :: E |> {{ Φ }} -∗
        eff l -∗ WP e <| E |> {{ y, Φ y }}.
  Proof.
    iIntros (?) "He Hl [%HND HE]".
    iDestruct (distinct_eff_name with "Hl HE") as "%Hnot_in".
    iApply fupd_ewp. 
    iMod (pointsto_persist with "Hl") as "Hl".
    iSpecialize ("He" with "[Hl HE]").
    { rewrite fmap_cons //=.
      iSplit.
      iPureIntro. by apply NoDup_cons.
      rewrite Eff_cons //=. by iFrame.
    }
    iModIntro.
    (* Proof by monotonicity of [ewp]. *)
    iApply (ewp_strong_mono with "He");
    first done;
    last by iIntros "!#" (y) "?".
    by iApply EFF_introduce.
  Qed.

End effects.


(** Shallow Handler. *)

(* Reasoning rule for installing a shallow handler. *)

Section shallow_handler.
  Context `{!heapG Σ}.

  (* Handler judgment. *)
  Definition is_shandler :
    (expr -d> expr -d> eff_label -d>
       pure_iEff Σ -d> eff_list Σ -d> eff_list Σ -d>
         (_ -d> iPropO Σ) -d> (_ -d> iPropO Σ) -d> iPropO Σ)
    := (λ h r l Ψ E E' Φ Φ',
   (* Return branch. *)
   □ (∀ (v : val),
      Φ v -∗ WP (r v) <| E' |> {{ Φ' }}) ∧

   (* Effect branch. *)
   □ (∀ (v k : val),
      protocol_agreement v Ψ (λ w,
        WP (k w) <| (l, Ψ) :: E |> {{ Φ }}) -∗
      WP (h v k) <| E' |> {{ Φ' }}))%I.

  (* Reasoning rule. *)
  Lemma wp_shandle (l : eff_label) E Ψ Ψ' Φ Φ' (client h r : val) :
    let E' := (l, Ψ') :: E in

    WP client #() <| (l, Ψ) :: E |> {{ Φ }} -∗
      is_shandler h r l Ψ E E' Φ Φ' -∗
        WP (shandle #l client h r) <| E' |> {{ Φ' }}.
  Proof.
    intros ?. iIntros "Hclient [#Hr #Hh] [%HND #HEff]".
    iSpecialize ("Hclient" with "[HEff]"). { by iSplit. }
    assert (l ∉ E.*1) as Hnot_in. { by inversion HND. }
    iLöb as "IH" forall (client).
    unfold shandle. do 10 ewp_value_or_step.
    iApply (ewp_try_with with "Hclient [Hh]").
    iSplit.
    (* Return branch. *)
    { iIntros (v) "HΦ !>". ewp_pure_steps.
      iApply ("Hr" with "HΦ"); by iSplit.
    }
    (* Effect branch. *)
    { iIntros (v k) "Hprot !>".
      rewrite EFF_agreement.
      iDestruct "Hprot" as (l' v' Ψ'' Q) "(-> & (% & HΨ'') & #Hk)".
      revert H; rewrite elem_of_cons or_comm; intro Hl'.
      ewp_pure_steps; [done|].
      case (decide (l = l')) as [->|Hneq].
      { rewrite bool_decide_eq_true_2; [|done].
        case Hl'.
        { intro Hin. cut (l' ∈ E.*1); [done|].
          by apply (elem_of_list_fmap_1 fst _ _ Hin).
        }
        { injection 1. intros ->. ewp_pure_steps.
          iApply ("Hh" with "[HΨ'']"); [|by iSplit].
          iExists Q. iFrame.
          iIntros "!#" (w) "HQ _".
          by iApply ("Hk" with "HQ").
        }
      }    
      { rewrite bool_decide_eq_false_2; [|by injection 1; intros ->].
        ewp_pure_steps.
        iApply ewp_eff.
        rewrite EFF_agreement.
        iExists l', v', Ψ'', Q. repeat (iSplit; try done).
        { iPureIntro. by case Hl' as [|]; set_solver. }
        iIntros "!#" (w) "HQ".
        iNext. iApply ewp_value. do 5 ewp_value_or_step.
        iApply "IH". ewp_pure_steps.
        by iApply "Hk".
      }
    }
  Qed.

End shallow_handler.


(** Deep Handler. *)

(* Reasoning rule for deep handlers. *)

Section deep_handler.
  Context `{!heapG Σ}.

  (* Handler judgment. *)
  Definition is_handler :
    (expr -d> expr -d> iEff Σ -d> eff_list Σ -d> (_ -d> iPropO Σ) -d>
       (_ -d> iPropO Σ) -d> iPropO Σ)
    := (λ h r Ψ E' Φ Φ',
   (* Return branch. *)
   □ (∀ (v : val),
      Φ v -∗ WP (r v) <| E' |> {{ Φ' }}) ∧

   (* Effect branch. *)
   □ (∀ (v k : val),
      protocol_agreement v Ψ (λ w,
        WP (k w) <| E' |> {{ Φ' }}) -∗
      WP (h v k) <| E' |> {{ Φ' }}))%I.

  (* Reasoning rule. *)
  Lemma wp_handle (l : eff_label) E Ψ Ψ' Φ Φ' (client h r : val) :
    let E' := (l, Ψ') :: E in

    WP client #() <| (l, Ψ) :: E |> {{ Φ }} -∗
      is_handler h r Ψ E' Φ Φ' -∗
        WP (handle #l client h r) <| E' |> {{ Φ' }}.
  Proof.
    intros ?. iIntros "Hclient [#Hr #Hh]".
    unfold handle.
    do 2 wp_value_or_step.
    iLöb as "IH" forall (client).
    wp_pure_steps.
    iApply (wp_shandle with "Hclient [Hh]").
    iSplit; iModIntro.
    (* Return branch. *)
    { iIntros (v) "HΦ". wp_pure_steps.
      iApply ("Hr" with "HΦ"); by iSplit.
    }
    (* Effect branch. *)
    { iIntros (v k) "Hprot".
      wp_pure_steps.
      iApply "Hh".
      iApply (protocol_agreement_mono with "Hprot").
      iIntros "!#" (w) "Hk". do 3 wp_value_or_step.
      iApply "IH". by wp_pure_steps.
    }
  Qed.

End deep_handler.


(** Catch All. *)

(* Reasoning rule for catch-all handlers. *)

Section catch_all.
  Context `{!heapG Σ}.

  (* Catch-all judgment. *)
  Definition is_catch_all_handler :
    (expr -d> expr -d> eff_list Σ -d> (_ -d> iPropO Σ) -d>
       (_ -d> iPropO Σ) -d> iPropO Σ)
    := (λ h r E Φ Φ',
   (* Return branch. *)
   □ (∀ (v : val),
      Φ v -∗ WP (r v) <| E |> {{ Φ' }}) ∧

   (* Effect branch. *)
   □ (∀ Ψ (f x k : val),

      (* Specification of [f]. *)
      □ (∀ (x : val) Φ'',
         protocol_agreement x Ψ Φ'' -∗
           WP (f x) <| E |> {{ Φ'' }}) -∗

      (* Joint specification of [x] and [k]. *)
      protocol_agreement x Ψ (λ w,
        WP (k w) <| E |> {{ Φ' }}) -∗

      WP (h f x k) <| E |> {{ Φ' }}))%I.

  Lemma ewp_catch_all E Φ Φ' (client h r : val) :
    EWP client #() <| EFF E |> {{ Φ }} -∗
      is_catch_all_handler h r E Φ Φ' -∗
        WP (catch_all client h r) <| E |> {{ Φ' }}.
  Proof.
    iIntros "Hclient [#Hr #Hh] [%HND #HEff]".
    iLöb as "IH" forall (client).
    unfold catch_all. do 7 ewp_value_or_step.
    iApply (ewp_try_with with "Hclient").
    iSplit.
    (* Return branch. *)
    { iIntros (v) "HΦ !>". ewp_pure_steps.
      iApply ("Hr" with "HΦ"); by iSplit.
    }
    (* Effect branch. *)
    { iIntros (x k) "Hprot !>".
      rewrite EFF_agreement.
      iDestruct "Hprot" as (l' v' Ψ Q) "(-> & (% & HΨ'') & #Hk)".
      ewp_pure_steps.
      iApply ("Hh" $! Ψ _ v' _ with "[] [HΨ'']"); last by iSplit.
      - iIntros "!#" (x Φ'') "Hprot". wp_pure_steps.
        by iApply (wp_perform with "Hprot").
      - iExists Q. iFrame.
        iIntros "!#" (w) "HQ _". do 3 ewp_value_or_step.
        iApply "IH". ewp_pure_steps.
        by iApply "Hk".
    }
  Qed.

  Lemma wp_catch_all E Φ Φ' (client h r : val) :
    WP client #() <| E |> {{ Φ }} -∗
      is_catch_all_handler h r E Φ Φ' -∗
        WP (catch_all client h r) <| E |> {{ Φ' }}.
  Proof.
    iIntros "Hclient Hrh #HLabels".
    iSpecialize ("Hclient" with "HLabels").
    by iApply (ewp_catch_all with "Hclient Hrh").
  Qed.

End catch_all.


(** Try Finally. *)

Section try_finally.
  Context `{!heapG Σ}.

  Lemma wp_try_finally (e₁ e₂ : val) E Φ :
    WP e₁ #() <| E |> {{ _, True }} -∗
      WP e₂ #() <| [] |> {{ Φ }} -∗
        WP (try_finally e₁ e₂) <| E |> {{ Φ }}.
  Proof.
    iIntros "He₁ He₂ #HL".
    iSpecialize ("He₁" with "HL").
    iSpecialize ("He₂" with "[]").
    { by iApply EffLabels_nil. }
    unfold try_finally. ewp_pure_steps.
    iApply (ewp_try_with with "He₁").
    iSplit.
    { iIntros (y) "Hy !>".
      ewp_pure_steps. ewp_bind_rule.
      iApply (ewp_mono_pure' _ _ Φ with "[He₂]").
      - iApply (ewp_strong_mono with "He₂"). done.
        { by iApply EFF_empty. } { by auto. }
      - iIntros (v) "Hv !>". simpl.
        ewp_pure_steps.
        by iFrame.
    }
    { iIntros (v k) "Hk !>". ewp_pure_steps.
      iApply (ewp_strong_mono with "He₂"). done.
      { iApply (iEff_le_trans _ ⊥%ieff).
        + by iApply EFF_empty.
        + by iApply iEff_le_bottom.
      }
      { by auto. }
    }
  Qed.

End try_finally.


(** Dynamic Wind *)

(* Reasoning rule for [dynamic_wind]. *)

Section dynamic_wind.
  Context `{!heapG Σ}.

  Lemma wp_dynamic_wind (pre body post : val) E Φ :
    □ WP pre #() <| [] |> {{ _, True }} -∗
      □ WP post #() <| [] |> {{ _, True }} -∗
        WP body #() <| E |> {{ Φ }} -∗
          WP dynamic_wind pre body post <| E |> {{ Φ }}.
  Proof.
    iIntros "#Hpre #Hpost Hbody".
    unfold dynamic_wind.
    wp_pure_steps.
    wp_bind_rule. simpl.
    iApply (wp_mono_pure with "[Hbody] Hpre").
    iIntros (w) "_ !>".
    wp_pure_steps.
    iApply (wp_catch_all with "Hbody").
    iSplit.
    { iIntros "!#" (x) "Hx".
      wp_pure_steps. wp_bind_rule.
      iApply (wp_mono_pure with "[Hx] Hpost").
      iIntros (?) "_ !>". simpl. by wp_pure_steps.
    }
    { iIntros "!#" (Ψ do' x k) "#Hdo Hprot".
      wp_pure_steps. wp_bind_rule.
      iApply (wp_mono_pure with "[Hprot] Hpost").
      iIntros (?) "_ !>". simpl. wp_pure_steps.
      wp_bind_rule. simpl.
      iApply "Hdo".
      iApply (protocol_agreement_mono with "Hprot").
      iIntros "!#" (w') "Hk". wp_pure_steps.
      wp_bind_rule. simpl.
      iApply (wp_mono_pure with "[Hk] Hpre").
      iIntros (?) "_ !>". simpl. by wp_pure_steps.
    }
  Qed.

End dynamic_wind.
