(* maps_and_sets.v *)

(* This file contains general-purpose lemmas about maps and sets. *)

From stdpp Require Import gmap gmultiset base.
From logic Require Import lang.


(** * Properties of [filter]. *)

Section filter.

  (* If we select the keys of a map [m] that satisfy a predicate [P],
     then the result is a map whose domain is equal to selecting the
     elements that satisfy [P] from the domain of the original map [m]. *)
  Lemma dom_filter_dom
    {K A} `{Countable K, EqDecision A} (m : gmap K A)
    (P : K → Prop) `{!∀ x, Decision (P x)} :
    dom (filter (λ '(k, _), P k) m) = filter P (dom m).
  Proof.
    rewrite (dom_filter_L _ _ (filter P (dom m))). done.
    intro i. rewrite elem_of_filter elem_of_dom /is_Some.
    split; naive_solver.
  Qed.

  (* The result of selecting elements from [Y] that satisfy membership to [X]
     is the intersection between [X] and [Y]. *)
  Lemma filter_elem_of {A} `{Countable A} (X Y : gset A) :
    filter (.∈ X) Y = X ∩ Y.
  Proof.
    apply set_eq. intro x.
    by rewrite elem_of_intersection elem_of_filter.
  Qed.

  Corollary filter_elem_of' {A} `{Countable A} (X Y : gset A) :
    X ⊆ Y →
      filter (.∈ X) Y = X.
  Proof. rewrite filter_elem_of. set_solver. Qed.

End filter.


(** * Properties of [binder_delete]. *)

Section binder_delete.

  (* [binder_delete] commutes with [fmap]. *)
  Lemma binder_delete_fmap {A B} `{EqDecision A, EqDecision B}
        (x : binder) (f : A → B) (m : gmap string A) :
    binder_delete x (f <$> m) = f <$> (binder_delete x m).
  Proof. destruct x; [done|]. by rewrite fmap_delete. Qed.

End binder_delete.


(** * Properties of [elements]. *)

Section elements.

  (* The concatenation of elements from [A] with the elements
     from [B] covers the elements from [A ∪ B]. *)
  Lemma elements_subseteq K `{Countable K} (A B : gset K) :
    elements (A ∪ B) ⊆ elements A ++ elements B.
  Proof. set_solver. Qed.

End elements.


(** * Properties of [(map_)union_with]. *)

Section union_with.

  (* The domain of merging [M] and [M'] according to the merging
     function [f] is included in the union of the domains of [M] and [M']
     regardless of [f]. *)
  Lemma dom_union_with_subseteq
    {K A} `{Countable K, EqDecision A} f (M M' : gmap K A) :
    dom (union_with f M M') ⊆ (dom M) ∪ (dom M').
  Proof.
    intros k. rewrite elem_of_union !elem_of_dom lookup_union_with.
    intros [a Hlkp]. revert Hlkp.
    by case_eq (M  !! k); (case_eq (M' !! k)); try auto.
  Qed.

  (* The converse is true if the merging function [f]
     never discards elements. *)
  Lemma subseteq_dom_union_with
    {K A} `{Countable K, EqDecision A} f (M M' : gmap K A) :
    (∀ a a', f a a' ≠ None) →
      (dom M) ∪ (dom M') ⊆ dom (union_with f M M').
  Proof.
    intros Heq. intros k.
    rewrite elem_of_union !elem_of_dom lookup_union_with.
    intros [[a ->]|[a' ->]];
    [case_eq (M' !! k)|case_eq (M !! k)]; simpl; try auto;
    [intros a'|intros a];
    case_eq (f a a'); specialize (Heq a a'); auto; by intros ?.
  Qed.

End union_with.


(** * Miscellaneous. *)

Section misc.

  (* [fmap] commutes with [lookup_total] if the index [i] is in the domain
     of the map [m].

     If [i] is not in [dom _ m], then the total lookup returns a default
     element both on the left-hand side and on the right-hand side of the
     equation. On the left-hand side, the element is an inhabitant from [B],
     whereas, on the right-hand side, the element is an inhabitant from [A].
     The equation would hold if the function [f] maps [A]'s inhabitant
     to [B]'s inhabitant. However, this is not true in general. *)
  Lemma lookup_total_fmap {K A B}
    `{Countable K, EqDecision A, EqDecision B} `{!Inhabited A, !Inhabited B}
    (f : A → B) (m : gmap K A) (i : K) :
    i ∈ dom m →
      (f <$> m) !!! i = f (m !!! i).
  Proof.
    rewrite elem_of_dom.
    intros [x Hx].
    rewrite (lookup_total_correct m i x Hx).
    apply lookup_total_correct.
    by rewrite lookup_fmap Hx.
  Qed.

  (* If the domain of a map [m] can be partitioned into two sets [D1] and [D2],
     then the map itself can be partitioned into two maps [m1] and [m2] of
     domains [D1] and [D2] respectively. *)
  Lemma split_map {K A} `{Countable K, EqDecision A}
    (m : gmap K A)
    (D1 D2 : gset K) :
    dom m = D1 ∪ D2 →
      D1 ## D2 →
        ∃ m1 m2,
          m = m1 ∪ m2          ∧
          dom m1 = D1 ∧
          dom m2 = D2.
  Proof.
    rewrite elem_of_disjoint.
    intros Hdom Hdisj.
    exists (filter (λ '(k, _), k ∈ D1) m).
    exists (filter (λ '(k, _), k ∉ D1) m).
    rewrite map_filter_union_complement.
    repeat split; try done.
    - rewrite dom_filter_dom filter_elem_of'; set_solver.
    - rewrite (map_filter_strong_ext_1 _ (λ '(k, _), k ∈ D2) _ m).
      + by rewrite dom_filter_dom filter_elem_of'; set_solver.
      + intros ??.
        split; intros [? ?]; split; try done.
        * generalize (elem_of_dom_2 _ _ _ H1).
          rewrite Hdom. set_solver.
        * intros ?. by apply (Hdisj i).
  Qed.

  (* Rewrite union in terms of a disjoint union. *)
  Lemma gmultiset_union_disj_union `{Countable A }(X Y : gmultiset A) :
    X ∪ Y = X ⊎ (Y ∖ (X ∩ Y)).
  Proof. multiset_solver. Qed.

End misc.
