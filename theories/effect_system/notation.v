(* notation.v *)

From logic Require Export lang notation labeled_effects.
From effect_system Require Export types.

Declare Scope ty_scope.
Bind Scope ty_scope with ty.
Bind Scope ty_scope with row.
Delimit Scope ty_scope with ty.

Notation "⊥" := TBot : ty_scope.
Notation "⊤" := TTop : ty_scope.
Notation "()" := TUnit : ty_scope.
Notation "'ℤ'" := TInt : ty_scope.
Notation "'𝔹'" := TBool : ty_scope.
Notation "α '-{' a ; ρ '}->' β" := (TArr a α ρ β)
  (at level 100, a, ρ, β at level 200) : ty_scope.
Notation "α '-{' ρ '}->' β" := (TArr I α ρ β)
  (at level 100, ρ, β at level 200) : ty_scope.
Notation "α '--->' β" := (TArr P α [] β)
  (at level 100, β at level 200) : ty_scope.
Notation "α '--->ₚ' β" := (TArr P α [] β)
  (at level 100, β at level 200) : ty_scope.
Notation "∀T: α" := (TForall T α%ty)
  (at level 100, α at level 200) : ty_scope.
Notation "∀R: α" := (TForall R α%ty)
  (at level 100, α at level 200) : ty_scope.
Infix "*" := TProd : ty_scope.
Infix "+" := TSum : ty_scope.
Notation "'ref' α" := (TRef α)
  (at level 10, α at next level, right associativity): ty_scope.
Notation "'lst' α" := (TList α)
  (at level 10, α at next level, right associativity): ty_scope.

Notation tk e := (Rec V BAnon BAnon e) (only parsing).
Notation vl s := (Var L s) (only parsing).

Notation LetEff x e1 e2 := (App (Rec L BAnon x e2) e1) (only parsing).
Notation "'effect:' s 'in' e" := (LetEff s%binder (effect #()) e%E)
  (at level 200, s at level 1, e at level 200,
   format "'[' 'effect:'  s  'in'  '/' e ']'") : expr_scope.

Definition bind' : val := λ: "x" "y", "y" "x".
Definition Bind (e1 e2 : expr) : expr := bind' e1 e2.
Notation "'bind:' x := e1 'in' e2" := (Bind e1%E (Lam x%binder e2%E))
  (at level 200, x at level 1, e1, e2 at level 200,
   format "'[' 'bind:'  x  :=  '[' e1 ']'  'in'  '/' e2 ']'") : expr_scope.
Notation "'bind:' x := e1 'in' e2" := (Bind e1%E (LamV x%binder e2%E))
  (at level 200, x at level 1, e1, e2 at level 200,
   format "'[' 'bind:'  x  :=  '[' e1 ']'  'in'  '/' e2 ']'") : val_scope.

Definition Ret (e : expr) : expr := e.
Notation "'ret:' e" := (Ret e%E)
  (at level 200, e at level 200,
   format "'[' 'ret:'  e ']'") : expr_scope.

Definition RLamV' (e : expr) : val  := LamV BAnon e.
Definition RLam'  (e : expr) : expr := Lam  BAnon e.
Notation "'ΛR:' e" := (RLamV' e%E)
  (at level 200, e at level 200,
   format "'[' 'ΛR:'  e ']'") : val_scope.
Notation "'ΛR:' e" := (RLam' e%E)
  (at level 200, e at level 200,
   format "'[' 'ΛR:'  e ']'") : expr_scope.

Definition RApp' (e : expr) : expr := App e #()%V.
Notation "e '<R>'" := (RApp' e%E)
  (at level 10,
   format "'[' e '<R>' ']'") : expr_scope.

Definition TLamV' (e : expr) : val  := LamV BAnon e.
Definition TLam'  (e : expr) : expr := Lam  BAnon e.
Notation "'ΛT:' e" := (TLamV' e%E)
  (at level 200, e at level 200,
   format "'[' 'ΛT:'  e ']'") : val_scope.
Notation "'ΛT:' e" := (TLam' e%E)
  (at level 200, e at level 200,
   format "'[' 'ΛT:'  e ']'") : expr_scope.

Definition TApp' (e : expr) : expr := App e #()%V.
Notation "e '<T>'" := (TApp' e%E)
  (at level 10,
   format "'[' e '<T>' ']'") : expr_scope.

