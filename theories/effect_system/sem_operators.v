(* sem_operators.v *)

(* This file is taken from the Iris tutorial @POPL'20:

       https://gitlab.mpi-sws.org/iris/tutorial-popl20

   The file specifies unary and binary operators in terms of semantic types,
   and proves that the operators of the language satisfy such specifications.
   If, instead of providing their implementation, we added these operators as
   constants of the language, then we could include such specifications as axioms.
*)

From effect_system Require Export interp.


(** * Unboxed Semantic Types. *)

(* An unboxed seamtic type is a semantic type [A] that contains only unboxed
   values. *)

Class SemTyUnboxed `{!heapG Σ} (A : sem_ty Σ) :=
  sem_ty_unboxed v :
    A v -∗ ⌜ val_is_unboxed v ⌝.

Section sem_unboxed.
  Context `{!heapG Σ}.
  Implicit Types A B : sem_ty Σ.

  Global Instance sem_ty_unit_unboxed : SemTyUnboxed ().
  Proof. by iIntros (v ->). Qed.
  Global Instance sem_ty_bool_unboxed : SemTyUnboxed sem_ty_bool.
  Proof. iIntros (v). by iDestruct 1 as (b) "->". Qed.
  Global Instance sem_ty_int_unboxed : SemTyUnboxed sem_ty_int.
  Proof. iIntros (v). by iDestruct 1 as (i) "->". Qed.
  Global Instance sem_ty_ref_unboxed A : SemTyUnboxed (ref A).
  Proof. iIntros (v). by iDestruct 1 as (i ->) "?". Qed.
End sem_unboxed.


(** * Semantic Operator Typing. *)

(* Specification of binary, or unary operators. *)

Class SemTyUnOp `{!heapG Σ} (op : un_op) (A B : sem_ty Σ) :=
  sem_ty_un_op v :
    A v -∗ ∃ w, ⌜ un_op_eval op v = Some w ⌝ ∧ B w.

Class SemTyBinOp `{!heapG Σ} (op : bin_op) (A1 A2 B : sem_ty Σ) :=
  sem_ty_bin_op v1 v2 :
    A1 v1 -∗ A2 v2 -∗ ∃ w, ⌜ bin_op_eval op v1 v2 = Some w ⌝ ∧ B w.


(** * Correctness of Operators. *)

Section sem_operators.
  Context `{!heapG Σ}.
  Implicit Types A B : sem_ty Σ.

  (* Unary operators. *)

  Global Instance sem_ty_un_op_int op : SemTyUnOp op sem_ty_int sem_ty_int.
  Proof. iIntros (?). iDestruct 1 as (i) "->". destruct op; eauto. Qed.
  Global Instance sem_ty_un_op_bool : SemTyUnOp NegOp sem_ty_bool sem_ty_bool.
  Proof. iIntros (?). iDestruct 1 as (i) "->". eauto. Qed.

  (* Binary operators. *)

  Global Instance sem_ty_bin_op_eq A : SemTyBinOp EqOp A A sem_ty_bool.
  Proof.
    iIntros (v1 v2) "A1 A2". rewrite /bin_op_eval /sem_ty_car /=.
    iExists #(bool_decide (v1 = v2)).
    by iSplit; [|iExists (bool_decide (v1 = v2))].
  Qed.
  Global Instance sem_ty_bin_op_arith op :
    TCElemOf op [PlusOp; MinusOp; MultOp; QuotOp; RemOp;
                 AndOp; OrOp; XorOp; ShiftLOp; ShiftROp] →
    SemTyBinOp op sem_ty_int sem_ty_int sem_ty_int.
  Proof.
    iIntros (? v1 v2); iDestruct 1 as (i1) "->"; iDestruct 1 as (i2) "->".
    repeat match goal with H : TCElemOf _ _ |- _ => inversion_clear H end;
      rewrite /sem_ty_car /=; eauto.
  Qed.
  Global Instance sem_ty_bin_op_compare op :
    TCElemOf op [LeOp; LtOp] →
    SemTyBinOp op sem_ty_int sem_ty_int sem_ty_bool.
  Proof.
    iIntros (? v1 v2); iDestruct 1 as (i1) "->"; iDestruct 1 as (i2) "->".
    repeat match goal with H : TCElemOf _ _ |- _ => inversion_clear H end;
      rewrite /sem_ty_car /=; eauto.
  Qed.
  Global Instance sem_ty_bin_op_bool op :
    TCElemOf op [AndOp; OrOp; XorOp] →
    SemTyBinOp op sem_ty_bool sem_ty_bool sem_ty_bool.
  Proof.
    iIntros (? v1 v2); iDestruct 1 as (i1) "->"; iDestruct 1 as (i2) "->".
    repeat match goal with H : TCElemOf _ _ |- _ => inversion_clear H end;
      rewrite /sem_ty_car /=; eauto.
  Qed.
End sem_operators.
