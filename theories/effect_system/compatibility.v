(* compatibility.v *)

(* This file contains the proof of the compatibility lemmas, that is, the proof
   that the typing rules preserve their interpretation as specifications
   written in Separation Logic given in the file [interp.v].
*)

From logic Require Import tactics.
From effect_system Require Export subst_map interp sem_operators rules interp.
From lib Require Import lists maps_and_sets.


(** * Values. *)

Section values.
  Context `{!heapG Σ}.

  (** Ground values. *)

  Lemma Unit_sem_typed {Δ Γ a ρ} : ⊢ (Δ .| Γ .|={a}= #() : ρ : ()%ty).
  Proof. by iIntros (????) "!# _ _"; iApply mwp_value. Qed.

  Lemma Bool_sem_typed {Δ Γ a ρ} (b : bool) :
    ⊢ (Δ .| Γ .|={a}= #b : ρ : TBool).
  Proof. by iIntros (????) "!# _ _"; iApply mwp_value; iExists b. Qed.

  Lemma Int_sem_typed {Δ Γ a ρ} (i : Z) : ⊢ (Δ .| Γ .|={a}= #i : ρ : TInt).
  Proof. by iIntros (????) "!# _ _"; iApply mwp_value; iExists i. Qed.

  (** Lists. *)

  Lemma Nil_sem_typed {Δ Γ a ρ τ} : ⊢ (Δ .| Γ .|={a}= InjR #() : ρ : lst τ).
  Proof.
    iIntros (????) "!# _ _". simpl.
    mwp_pure_steps. iExists []; by iSplit.
  Qed.

  Lemma Cons_sem_typed {Δ Γ a ρ τ x xs} :
    Δ .| Γ .|={a}= x : ρ : τ -∗
      Δ .| Γ .|={a}= xs : ρ : lst τ -∗
        Δ .| Γ .|={a}= InjL (x, xs) : ρ : lst τ.
  Proof.
    iIntros "#Hx #Hxs" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx InjLCtx (ConsCtx (PairRCtx _) _))). done.
    iApply mwp_mono; [| iApply ("Hxs" with "[//] HΓ")].
    iIntros "!#" (xsv) "[%us [-> #Hus]]". fold interp._ty.
    iApply (mwp_bind (ConsCtx InjLCtx (ConsCtx (PairLCtx _) _))). done.
    iApply mwp_mono; [| iApply ("Hx" with "[//] HΓ")].
    iIntros "!#" (xv) "#Hxv". simpl.
    mwp_pure_steps.
    iExists (xv :: us).
    by iSplit; [|iSplit].
  Qed.

End values.


(** * Effects. *)

Section effects.
  Context `{!heapG Σ}.

  (* LetEff - Introduce effect name in the context. *)
  Lemma LetEff_sem_typed {Δ Γ a e} {s : string} {ρ α} :
    let Δ' := <[s:=()]> Δ in
    let Abs_ρ := EAbs s :: ρ in

    vars._fresh s Γ ρ α →
      Δ' .| Γ .|={a}= e : Abs_ρ : α -∗
        Δ .| Γ .|={I}= (effect: s in e) : ρ : α.
  Proof.
    intros ?? Hvars.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    destruct Hvars as (Hnotin_Γ & Hvars_env &
                       Hvars_row & Hvars_ty).
    iDestruct (env_sem_typed_dom' with "HΓ") as %Hdomvs.
    iApply (wp_bind (ConsCtx (AppRCtx _) _)). done. simpl.
    iApply wp_effect.
    iIntros (l) "Hl".
    iApply (wp_introduce with "[] Hl").
    fold interp._ty. wp_pure_steps.
    rewrite subst_subst_map.
    rewrite -subst_map_insert -fmap_insert.
    set δ' := (<[s:=l]> δ : gmap string eff_label).
    set vs' := delete s vs.
    iSpecialize ("He" $! η ξ δ' vs with "[] [HΓ]").
    { iPureIntro. by set_solver. }
    { by rewrite interp_env_vars_env. }
    { rewrite interp_row_cons //=.
      rewrite lookup_total_insert //=.
      rewrite interp_row_vars_row; [|done].
      rewrite interp_ty_vars_ty; [|done].
      iApply mwp_wp.
      iApply (mwp_weaken with "[] He").
      by iApply (SIG_le_bottom η ξ δ TTop).
    }
  Qed.

  (* Perform. *)
  Lemma Perform_sem_typed {Δ Γ a e s ρ} α {β} :
    s ∈ dom Δ →
      ESig s α β ∈ ρ →
        Δ .| Γ .|={a}= e : ρ : α -∗
          Δ .| Γ .|={a}= (perform (Var L s) e) : ρ : β.
  Proof.
    iIntros (Hs Hsig) "#He".
    iIntros (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    rewrite lookup_lookup_total_dom; [|set_solver].
    rewrite lookup_total_fmap //=; [|set_solver].
    iApply (mwp_bind (ConsCtx (AppRCtx _) _)). done. simpl.
    iApply mwp_mono; [|by iApply "He"].
    { iIntros "!#" (a') "#Ha'".
      iApply mwp_perform.
      { by apply interp_row_elem_of. }
      rewrite SIG_agreement.
      by iExists a'; repeat (iSplit; auto).
    }
  Qed.

  (* Shallow handler. *)
  Lemma SHandle_sem_typed {Δ Γ a s e h r ρ} α β τ {α' β' τ'} :
    let sρ  := ESig s α  β  :: ρ in
    let sρ' := ESig s α' β' :: ρ in

    s ∈ dom Δ →
      Δ .| Γ .|={a}= e : sρ' : (() -{a; sρ}-> τ)%ty -∗
        Δ .| Γ .|={a}= h : sρ' : (α -{a; [] }-> (β -{a; sρ}-> τ) -{a; sρ'}-> τ')%ty -∗
          Δ .| Γ .|={a}= r : sρ' : (τ -{a; sρ'}-> τ')%ty -∗
            Δ .| Γ .|={a}= (shandle (Var L s) e h r) : sρ' : τ'.
  Proof.
    intros ??.
    iIntros (Hs) "#He #Hh #Hr".
    iIntros (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    rewrite lookup_lookup_total_dom; [|set_solver].
    rewrite lookup_total_fmap //=; [|set_solver].
    iApply (mwp_bind (ConsCtx (AppRCtx _) _)). done. simpl.
    iApply mwp_mono; [|
    by iApply ("Hr" with "[//] HΓ")].
    iIntros "!#" (rv) "#Hrv". fold interp._ty.
    iApply (mwp_bind (ConsCtx (AppLCtx _)
                     (ConsCtx (AppRCtx _) _))). done. simpl.
    iApply mwp_mono; [|
    by iApply ("Hh" with "[//] HΓ")].
    iIntros "!#" (hv) "#Hhv". fold interp._ty.
    iApply (mwp_bind (ConsCtx (AppLCtx _)
                     (ConsCtx (AppLCtx _)
                     (ConsCtx (AppRCtx _) _)))). done. simpl.
    iApply mwp_mono; [|
    by iApply ("He" with "[//] HΓ")].
    iIntros "!#" (ev) "#Hev". fold interp._ty.
    rewrite !interp_row_cons. simpl.
    iApply mwp_shandle.
    { by iApply "Hev". }
    { rewrite is_mixed_shandler_unfold.
      iSplit; iModIntro.
      { by iApply "Hrv". }
      { iIntros (v k). rewrite SIG_agreement.
        iIntros "[%a' (-> & #Ha' & #Hk)]".
        iApply (mwp_bind (ConsCtx (AppLCtx _) _)). done. simpl.
        iApply mwp_mono; [|
        iApply mwp_eff_frame; [by apply submseteq_nil_l|];
        iApply ("Hhv" with "Ha'")].
        iIntros "!#" (hav) "#Hhav".
        iApply "Hhav".
        iIntros "!#" (b) "#Hbv".
        by iApply "Hk".
      }
    }
  Qed.

  (* Try-finally statement. *)
  Lemma TryFinally_sem_typed {Δ Γ a e₁ e₂ ρ α} :
    Δ .| Γ .|={a}= e₁ : ρ : (() -{a; ρ}-> ())%ty -∗
      Δ .| Γ .|={a}= e₂ : ρ : (() -{a; [] }-> α)%ty -∗
        Δ .| Γ .|={a}= (try_finally e₁ e₂) : ρ : α.
  Proof.
    iIntros "#He₁ #He₂".
    iIntros (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx (AppRCtx _) _)). done. simpl.
    iApply mwp_mono; [|
    by iApply ("He₂" with "[//] HΓ")].
    iIntros "!#" (ev₂) "#Hev₂". fold interp._ty.
    iApply (mwp_bind (ConsCtx (AppLCtx _)
                     (ConsCtx (AppRCtx _) _))). done. simpl.
    iApply mwp_mono; [|
    by iApply ("He₁" with "[//] HΓ")].
    iIntros "!#" (ev₁) "#Hev₁". fold interp._ty.
    iApply mwp_try_finally.
    - by iApply mwp_mono; [|iApply "Hev₁"]; auto.
    - by iApply "Hev₂".
  Qed.

  (* Dynamic-wind instruction. *)
  Lemma DynamicWind_sem_typed {Δ Γ a e₁ e₂ e₃ ρ α} :
    Δ .| Γ .|={a}= e₁ : ρ : (() -{a; [] }-> ())%ty -∗
      Δ .| Γ .|={a}= e₂ : ρ : α -∗
        Δ .| Γ .|={a}= e₃ : ρ : (() -{a; [] }-> ())%ty -∗
          Δ .| Γ .|={a}= (dynamic_wind e₁ (tk e₂) e₃) : ρ : α.
  Proof.
    iIntros "#He₁ #He₂ #He₃".
    iIntros (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx (AppRCtx _) _)). done. simpl.
    iApply mwp_mono; [|
    by iApply ("He₃" with "[//] HΓ")].
    iIntros "!#" (ev₃) "#Hev₃". fold interp._ty.
    mwp_pure_steps.
    iApply (mwp_bind (ConsCtx (AppLCtx _)
                     (ConsCtx (AppLCtx _)
                     (ConsCtx (AppRCtx _) _)))). done.
    iApply mwp_mono; [|
    by iApply ("He₁" with "[//] HΓ")].
    iIntros "!#" (ev₁) "#Hev₁". fold interp._ty. simpl.
    iApply mwp_dynamic_wind.
    { iModIntro. by iApply mwp_mono; [|iApply "Hev₁"]. }
    { iModIntro. by iApply mwp_mono; [|iApply "Hev₃"]. }
    { mwp_pure_steps. by iApply ("He₂" with "[//] HΓ"). }
  Qed.

  (* Remove effect name from the context. *)
  Lemma RemoveEff_sem_typed {Δ Γ e a} {s : string} {ρ τ} :
    let Δ' := <[s:=()]> Δ in

    Δ .| Γ .|={a}= e : ρ : τ -∗
      Δ' .| Γ .|={a}= e : ρ : τ.
  Proof.
    iIntros (?) "#He".
    iIntros (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply ("He" with "[] HΓ").
    iPureIntro. by set_solver.
  Qed.

End effects.


(** * Polymorphism. *)

Section polymorphism.
  Context `{!heapG Σ}.

  (** Value polymorphism. *)

  (* Introduction of value-polymorphic type. *)
  Lemma TLam_sem_typed {Δ Γ a e ρ α} :
    Δ .| (tvar_lift._ty 0) <$> Γ .|={P}= e : (tvar_lift._row 0 ρ) : α -∗
      Δ .| Γ .|={a}= e : ρ : (∀T: α).
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply pwp_mwp.
    iApply (@pwp_forall _ _ _ sem_ty_inhabited).
    iIntros "!#" (A).
    iSpecialize ("He" with "[//] [HΓ]").
    { by rewrite -(interp_env_tvar_lift_ty_0 A). }
    by rewrite (interp_tvar_lift_row_0 A).
  Qed.

  (* Elimination of value-polymorphic type. *)
  Lemma TApp_sem_typed {Δ Γ a e ρ α β} :
    Δ .| Γ .|={a}= e : ρ : (∀T: α) -∗
      Δ .| Γ .|={a}= e : ρ : tvar_subst._ty 0 β α.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iSpecialize ("He" with "[//] HΓ").
    iApply (mwp_mono with "[] He").
    iIntros "!#" (v) "#Hv".
    iSpecialize ("Hv" $! V⟦ η ; ξ; δ; β ⟧).
    fold interp._ty.
    by rewrite (interp_tvar_subst_ty_0 β η ξ δ α).
  Qed.

  (** Effect polymorphism. *)

  (* Introduction of effect-polymorphic type. *)
  Lemma RLam_sem_typed {Δ Γ a e ρ α} :
    Δ .| (rvar_lift._ty 0) <$> Γ .|={P}= e : (rvar_lift._row 0 ρ) : α -∗
      Δ .| Γ .|={a}= e : ρ : (∀R: α).
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply pwp_mwp.
    iApply (@pwp_forall _ _ _ sem_row_inhabited).
    iIntros "!#" (E).
    iSpecialize ("He" with "[//] [HΓ]").
    { by rewrite -(interp_env_rvar_lift_ty_0 E). }
    by rewrite (interp_rvar_lift_row_0 E).
  Qed.

  (* Elimination of effect-polymorphic type. *)
  Lemma RApp_sem_typed {Δ Γ a e ρ α ρ'} :
    Δ .| Γ .|={a}= e : ρ : (∀R: α) -∗
      Δ .| Γ .|={a}= e : ρ : rvar_subst._ty 0 ρ' α.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iSpecialize ("He" with "[//] HΓ").
    iApply (mwp_mono with "[] He").
    iIntros "!#" (v) "#Hv".
    iSpecialize ("Hv" $! R⟦ η ; ξ; δ; ρ' ⟧).
    fold interp._ty.
    by rewrite (interp_rvar_subst_ty_0 ρ' η ξ δ α).
  Qed.

End polymorphism.


(** * Reference type. *)

Section references.
  Context `{!heapG Σ}.

  (* Memory allocation - Introduction of a reference type. *)
  Lemma Alloc_sem_typed {Δ Γ a e ρ α} :
    Δ .| Γ .|={a}= e : ρ : α -∗
      Δ .| Γ .|={I}= ref e : ρ : ref α.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (wp_bind (ConsCtx AllocCtx _)). done.
    iApply wp_mono; [|iApply mwp_wp; iApply ("He" with "[//] HΓ")].
    iIntros "!#" (v) "#Hv !>". simpl.
    iApply wp_alloc.
    iIntros "!>" (l) "Hl".
    iMod (inv_alloc (tyN.@l) _
         (∃ v, l ↦ v ∗ V⟦ η ; ξ ; δ ; α ⟧ v)%I with "[Hl]") as "#Hinv".
    { iNext. by eauto. }
    iModIntro. iExists l. by auto.
  Qed.

  (* Load instruction. *)
  Lemma Load_sem_typed {Δ Γ a e ρ α} :
    Δ .| Γ .|={a}= e : ρ : ref α -∗
      Δ .| Γ .|={I}= Load e : ρ : α.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (wp_bind (ConsCtx LoadCtx _)). done.
    iApply wp_mono; [|iApply mwp_wp; iApply ("He" with "[//] HΓ")].
    iIntros (v) "!> [%l [-> #Hinv]]". fold interp._ty.
    iModIntro. simpl.
    iIntros "[% H]".
    iApply (ewp_atomic _ (⊤ ∖ ↑tyN.@l)).
    iMod (inv_acc _ (tyN.@l) with "Hinv") as "[Hl Hclose]"; first done.
    iModIntro.
    iDestruct "Hl" as (v) "[Hl #Hv]".
    iApply (ewp_load with "Hl"). iNext.
    iIntros "Hl !>". iMod ("Hclose" with "[Hl Hv]"); [|done].
    by iExists v; auto.
  Qed.

  (* Write instruction. *)
  Lemma Store_sem_typed {Δ Γ a₁ a₂ e₁ e₂ ρ} α :
    Δ .| Γ .|={a₁}= e₁ : ρ : ref α -∗
      Δ .| Γ .|={a₂}= e₂ : ρ : α -∗
        Δ .| Γ .|={I}= (e₁ <- e₂) : ρ : ().
  Proof.
    iIntros "#He1 #He2" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (wp_bind (ConsCtx (StoreRCtx _) _)). done.
    iApply wp_mono; [|iApply mwp_wp; iApply ("He2" with "[//] HΓ")].
    iIntros (w) "!# #Hw !>". simpl.
    iApply (wp_bind (ConsCtx (StoreLCtx _) _)). done.
    iApply wp_mono; [|iApply mwp_wp; iApply ("He1" with "[//] HΓ")].
    iIntros (r) "!# [%l [-> #Hinv]] !>". fold interp._ty. simpl.
    iIntros "_".
    iApply (ewp_atomic _ (⊤ ∖ ↑tyN.@l)).
    iMod (inv_acc _ (tyN.@l) with "Hinv") as "[Hl Hclose]"; first done.
    iModIntro.
    iDestruct "Hl" as (v) "[Hl #Hv]".
    iApply (ewp_store with "Hl"). iNext.
    iIntros "Hl !>". iMod ("Hclose" with "[Hl Hv]"); [|done].
    by iExists w; auto.
  Qed.

End references.


(** * Monotonicity. *)

Section monotonicity.
  Context `{!heapG Σ}.

  (* Monotonicity of rows. *)
  Lemma Mono_sem_typed {Δ Γ} a {a' e} b ρ α {ρ' α'} :
    a ≤A a' →
      ∅ ⊨ ρ ≤R ρ' @ b -∗
        ∅ ⊨ α ≤T α' -∗
          Δ .| Γ .|={a}= e : ρ  : α -∗
            Δ .| Γ .|={a'}= e : ρ' : α'.
  Proof.
    iIntros (HA) "#HR #HT #He".
    iIntros (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iDestruct ("HR" $! η ξ δ with "[]") as "(#Hle&#HL&_)".
    { by iApply interp_le_disj_ctx_empty. }
    iSpecialize ("HT" $! η ξ δ with "[]").
    { by iApply interp_le_disj_ctx_empty. }
    case a, a'; try done;
    iIntros "#HL'";
    iSpecialize ("HL" with "HL'");
    iSpecialize ("He" with "[//] HΓ HL").
    - iApply (ewp_strong_mono with "He"); try done.
      { by iIntros "!#" (?) "? !>"; iApply "HT". }
    - iApply pewp_ewp.
      by iApply (pewp_strong_mono with "He").
    - by iApply (pewp_strong_mono with "He").
  Qed.

  (* Weakening of a signature. *)
  Lemma Dismiss_sem_typed {Δ Γ a s e ρ α β τ} :
    let Abs_ρ := EAbs s :: ρ in
    let Pre_ρ := ESig s α β :: ρ in

    Δ .| Γ .|={a}= e : Abs_ρ : τ -∗
      Δ .| Γ .|={a}= e : Pre_ρ : τ.
  Proof.
    intros ??.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iSpecialize ("He" with "[//] HΓ").
    rewrite !interp_row_cons.
    iApply mwp_dismiss.
    iApply (mwp_weaken with "[] He").
    by iApply (SIG_le_bottom _ _ _ TTop).
  Qed.

End monotonicity.


(** * Residual rules. *)

Section residual.
  Context `{!heapG Σ}.

  (* Pure expression. *)
  Lemma Pure_sem_typed {Δ Γ a e ρ α} :
    Δ .| Γ .|={P}= e : ρ : α -∗
      Δ .| Γ .|={a}= e : ρ : α.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ //=".
    iApply pwp_mwp. by iApply ("He" $! η ξ δ).
  Qed.

  (* Variables. *)
  Lemma Var_sem_typed {Δ Γ x a ρ α} :
    Γ !! x = Some α →
      ⊢ (Δ .| Γ .|={a}= (Var V x) : ρ : α).
  Proof.
    intros Hlkp.
    iIntros (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iPoseProof (env_sem_typed_lookup with "HΓ") as (v ->) "HA".
    { by rewrite /interp._env lookup_fmap Hlkp. }
    by iApply mwp_value.
  Qed.

  (* Bottom type. *)
  Lemma Bot_sem_typed {Δ Γ a e ρ α} :
    Δ .| Γ .|={a}= e : ρ : ⊥ -∗
      Δ .| Γ .|={a}= e : ρ : α.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ".
    iApply mwp_mono; [| iApply ("He" with "[//] HΓ")].
    by iIntros "!#" (v) "Hfalse".
  Qed.

  (* Introduction of a product type. *)
  Lemma Prod_sem_typed {Δ Γ a e₁ e₂ ρ α β} :
    Δ .| Γ .|={a}= e₁ : ρ : α -∗
      Δ .| Γ .|={a}= e₂ : ρ : β -∗
        Δ .| Γ .|={a}= (e₁, e₂) : ρ : α * β.
  Proof.
    iIntros "#He1 #He2"(η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx (PairRCtx _) _)). done.
    iApply mwp_mono; [| iApply ("He2" with "[//] HΓ")].
    iIntros "!#" (b) "#Hb". simpl.
    iApply (mwp_bind (ConsCtx (PairLCtx _) _)). done.
    iApply mwp_mono; [| iApply ("He1" with "[//] HΓ")].
    iIntros "!#" (a') "#Ha'". simpl.
    mwp_pure_steps.
    iExists a', b. by repeat iSplit.
  Qed.

  (* Fst - Elimination of a product type. *)
  Lemma Fst_sem_typed {Δ Γ a e ρ α} β :
    Δ .| Γ .|={a}= e : ρ : α * β -∗
      Δ .| Γ .|={a}= Fst e : ρ : α.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx FstCtx _)). done.
    iApply mwp_mono; [| iApply ("He" with "[//] HΓ")].
    iIntros "!#" (ab) "[%a' [%b (-> & #Ha' & _)]]". simpl.
    by mwp_pure_steps.
  Qed.

  (* Snd - Elimination of a product type. *)
  Lemma Snd_sem_typed {Δ Γ a e ρ} α {β} :
    Δ .| Γ .|={a}= e : ρ : α * β -∗
      Δ .| Γ .|={a}= Snd e : ρ : β.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx SndCtx _)). done.
    iApply mwp_mono; [| iApply ("He" with "[//] HΓ")].
    iIntros "!#" (ab) "[%a' [%b (-> & _ & #Hb)]]". simpl.
    by mwp_pure_steps.
  Qed.

  (* Introduction of a sum type by left injection. *)
  Lemma InjL_sem_typed {Δ Γ a e ρ α β} :
    Δ .| Γ .|={a}= e : ρ : α -∗
      Δ .| Γ .|={a}= InjL e : ρ : α + β.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx InjLCtx _)). done.
    iApply mwp_mono; [| iApply ("He" with "[//] HΓ")].
    iIntros "!#" (a') "#Ha'". simpl.
    mwp_pure_steps.
    iLeft. by iExists a'; iSplit.
  Qed.

  (* Introduction of a sum type by right injection. *)
  Lemma InjR_sem_typed {Δ Γ a e ρ α β} :
      Δ .| Γ .|={a}= e : ρ : β -∗
        Δ .| Γ .|={a}= InjR e : ρ : α + β.
  Proof.
    iIntros "#He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx InjRCtx _)). done.
    iApply mwp_mono; [| iApply ("He" with "[//] HΓ")].
    iIntros "!#" (b) "#Hb". simpl.
    mwp_pure_steps.
    iRight. by iExists b; iSplit.
  Qed.

  (* Elimination of a sum type. *)
  Lemma Case_sem_typed {Δ Γ a e e₁ e₂ ρ α β τ} :
    Δ .| Γ .|={a}= e₁ : ρ : (α -{a; ρ}-> τ)%ty -∗
      Δ .| Γ .|={a}= e₂ : ρ : (β -{a; ρ}-> τ)%ty -∗
        Δ .| Γ .|={a}= e : ρ : α + β -∗
          Δ .| Γ .|={a}= Case e e₁ e₂ : ρ : τ.
  Proof.
    iIntros "#He1 #He2 #He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx (CaseCtx _ _) _)). done.
    iApply mwp_mono; [| iApply ("He" with "[//] HΓ")].
    iIntros "!#" (ab) "[[%a' [-> #Ha']]|[%b [-> #Hb]]]";
    fold interp._ty; simpl; mwp_pure_steps;
    iApply (mwp_bind (ConsCtx (AppLCtx _) _)); try done;
    iApply mwp_mono;
    try iApply ("He1" with "[//] HΓ");
    try iApply ("He2" with "[//] HΓ");
    iIntros "!#" (f) "Hf"; by iApply "Hf".
  Qed.

  (* Elimination of a list type. *)
  Lemma ListCase_sem_typed {Δ Γ a e e₁ e₂ ρ α τ} :
    Δ .| Γ .|={a}= e₁ : ρ : ((α * lst α) -{a; ρ}-> τ)%ty -∗
      Δ .| Γ .|={a}= e₂ : ρ : τ -∗
        Δ .| Γ .|={a}= e : ρ : lst α -∗
          Δ .| Γ .|={a}= Case e e₁ (Lam BAnon e₂) : ρ : τ.
  Proof.
    iIntros "#He1 #He2 #He" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx (CaseCtx _ _) _)). done.
    iApply mwp_mono; [| iApply ("He" with "[//] HΓ")].
    iIntros "!#" (xxs) "[%us [-> #Hus]]". fold interp._ty.
    case us as [|u us]; simpl; mwp_pure_steps.
    - by iApply ("He2" with "[//] HΓ").
    - iApply (mwp_bind (ConsCtx (AppLCtx _) _)); try done.
      iApply mwp_mono; last iApply ("He1" with "[//] HΓ").
      iIntros "!#" (f) "Hf". simpl.
      iApply "Hf".
      iExists u, (encode_list us). iSplit; [done|].
      iDestruct "Hus" as "[$ Hus]".
      iExists us. by iSplit.
  Qed.

  (* Application. *)
  Lemma App_sem_typed {Δ Γ a e₁ e₂ ρ} α {β} :
    Δ .| Γ .|={a}= e₁ : ρ : (α -{a; ρ}-> β) -∗
      Δ .| Γ .|={a}= e₂ : ρ  :  α -∗
        Δ .| Γ .|={a}= (e₁ e₂) : ρ : β.
  Proof.
   iIntros "#He1 #He2" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
   iApply (mwp_bind (ConsCtx (AppRCtx _) _)). done. simpl.
   iApply mwp_mono; [| iApply ("He2" with "[//] HΓ")].
   iIntros "!#" (v) "#Hv".
   iApply (mwp_bind (ConsCtx (AppLCtx _) _)). done. simpl.
   iApply mwp_mono; [| iApply ("He1" with "[//] HΓ")].
   iIntros "!#" (f) "Hf ".
   by iApply "Hf".
  Qed.

  (* If-else branching. *)
  Lemma If_sem_typed {Δ Γ a eb e₁ e₂ ρ α} :
    Δ .| Γ .|={a}= eb : ρ : 𝔹 -∗
      Δ .| Γ .|={a}= e₁ : ρ : α -∗
        Δ .| Γ .|={a}= e₂ : ρ : α -∗
          Δ .| Γ .|={a}= (if: eb then e₁ else e₂) : ρ : α.
  Proof.
    iIntros "#Heb #He1 #He2" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx (IfCtx _ _) _)). done. simpl.
    iApply mwp_mono; [| iApply ("Heb" with "[//] HΓ")].
    iIntros "!#" (b) "#[%b' ->]".
    by destruct b'; mwp_pure_steps; [
    iApply ("He1" with "[//] HΓ")|
    iApply ("He2" with "[//] HΓ")].
  Qed.

  (* Binary operation. *)
  Lemma BinOp_sem_typed {Δ Γ a op e₁ e₂ ρ} α β {σ} :
    ty_bin_op op α β σ →
      Δ .| Γ .|={a}= e₁ : ρ : α -∗
        Δ .| Γ .|={a}= e₂ : ρ : β -∗
          Δ .| Γ .|={a}= BinOp op e₁ e₂ : ρ : σ.
  Proof.
    intros Hbinop.
    iIntros "#He1 #He2" (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply (mwp_bind (ConsCtx (BinOpRCtx _ _) _)). done. simpl.
    iApply mwp_mono; [| iApply ("He2" with "[//] HΓ")].
    iIntros "!#" (b) "#Hb".
    iApply (mwp_bind (ConsCtx (BinOpLCtx _ _) _)). done. simpl.
    iApply mwp_mono; [| iApply ("He1" with "[//] HΓ")].
    iIntros "!#" (a') "#Ha'".
    set Va := V⟦ η ; ξ ; δ ; α ⟧.
    set Vb := V⟦ η ; ξ ; δ ; β ⟧.
    set Vs := V⟦ η ; ξ ; δ ; σ ⟧.
    cut (SemTyBinOp op Va Vb Vs); [|
    by destruct Hbinop; try apply _ ].
    intros ?.
    iPoseProof (sem_ty_bin_op with "Ha' Hb") as "[%c [%Hc Hc]]".
    mwp_pure_steps. apply Hc. done.
  Qed.

  (* Possibly recursive function definitions. *)
  Lemma Rec_sem_typed {Δ Γ f x a₁ a₂ e ρ α β} :
    let Γ' := binder_insert f (α -{a₁; ρ}-> β)%ty (binder_insert x α Γ) in

    Δ .| Γ' .|={a₁}= e : ρ : β -∗
      Δ .| Γ  .|={a₂}= (rec: f x := e) : [] : (α -{a₁; ρ}-> β).
  Proof.
    iIntros (?) "#He".
    iIntros (η ξ δ vs) "!# %Hdom #HΓ". simpl.
    iApply pwp_mwp.
    pwp_pure_steps.
    iLöb as "IH".
    iModIntro.
    iIntros (xv) "#Hx".
    mwp_pure_steps.
    rewrite -!subst_map_binder_insert_2.
    iApply ("He" with "[//]").
    rewrite /interp._env !binder_insert_fmap.
    iApply (env_sem_typed_insert with "IH").
    by iApply (env_sem_typed_insert with "Hx").
  Qed.

End residual.
