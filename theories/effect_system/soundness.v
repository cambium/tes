(* soundness.v *)

(* This file contains Tes's soundness theorem. *)

From logic Require Import tactics heap.
From iris.program_logic Require Import adequacy.
From effect_system Require Import mixed_weakestpre fundamental.

Section soundness.
  Context `{!heapPreG Σ}.

  Theorem soundness {a e τ}
    (Hty: ∅ .| ∅ .|-{a}- e : [] : τ) :
     ∀ σ e' σ',
       rtc erased_step ([e], σ) ([e'], σ') →
         not_stuck e' σ'.
  Proof.
    intros ??.
    apply (mwp_not_stuck a e σ (λ _, True%I))=>?.
    iPoseProof (fundamental Hty) as "Hty".
    set δ  := (∅ : gmap eff_name eff_label).
    set vs := (∅ : gmap string val).
    iSpecialize ("Hty" $! [] [] δ vs with "[] []").
    { iPureIntro. by rewrite !dom_empty_L. }
    { rewrite interp_env_empty.
      by iApply env_sem_typed_empty.
    }
    rewrite fmap_empty !subst_map_empty.
    rewrite /interp._row /interp._row_pre //=.
    iApply (mwp_mono with "[] Hty").
    by iIntros "!#" (?) "_".
  Qed.

End soundness.
